package com.rocketstudio.dev.loseit.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.rocketstudio.dev.loseit.DatabaseClass;
import com.rocketstudio.dev.loseit.R;

/**
 * Created by juliusherold on 02.04.17.
 */

public class SettingsDialog extends Dialog {

    EditText weight1,weight2,size1,size2;
    DatabaseClass databaseClass;
    Switch unitS;
    Context context;
    Button saveB;

    public SettingsDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.settigs_dialog);

        findViews();
        onClicks();
    }

    private void onClicks() {
        saveB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(weight1.getText()) || TextUtils.isEmpty(weight2.getText()) || TextUtils.isEmpty(size1.getText()) || TextUtils.isEmpty(size2.getText())){
                    if (TextUtils.isEmpty(weight1.getText())){
                        YoYo.with(Techniques.Shake).duration(700).playOn(weight1);
                    }if (TextUtils.isEmpty(weight2.getText())){
                        YoYo.with(Techniques.Shake).duration(700).playOn(weight2);
                    }if (TextUtils.isEmpty(size1.getText())){
                        YoYo.with(Techniques.Shake).duration(700).playOn(size1);
                    }if (TextUtils.isEmpty(size2.getText())){
                        YoYo.with(Techniques.Shake).duration(700).playOn(size2);
                    }
                }else {

                    int system;

                    if (unitS.isChecked()){
                        system = 1;
                    }else{
                        system = 0;
                    }

                    String gewicht = weight1.getText() + "." + weight2.getText() + ";" + weight1.getText() + "." + weight2.getText();
                    String groese = size1.getText() + "." + size2.getText();
                    String date = getTimeStamp() + ";" + getTimeStamp();

                    databaseClass = new DatabaseClass(getContext());
                    databaseClass.insertData(gewicht,date,2,1,1,15,groese,system);
                    dismiss();
                }
            }
        });

        unitS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (unitS.isChecked()){
                    weight1.setHint("kg");
                    weight2.setHint("gr");
                    size1.setHint("m");
                    size2.setHint("cm");
                }else {
                    weight1.setHint("lb");
                    weight2.setHint("oz");
                    size1.setHint("in");
                    size2.setHint("ft");
                }
            }
        });


    }

    public String getTimeStamp(){
        Time time = new Time(Time.getCurrentTimezone());
        time.setToNow();

        String datum = time.monthDay + ":" + time.month + ":" + time.year;

        return datum;
    }

    private void findViews() {
        unitS = (Switch) findViewById(R.id.unitS);
        weight1 = (EditText) findViewById(R.id.weight1);
        weight2 = (EditText) findViewById(R.id.weight2);
        size1 = (EditText) findViewById(R.id.size1);
        size2 = (EditText) findViewById(R.id.size2);
        saveB = (Button) findViewById(R.id.saveB);
    }
}
