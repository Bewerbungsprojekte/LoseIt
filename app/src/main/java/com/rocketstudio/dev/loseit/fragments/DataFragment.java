package com.rocketstudio.dev.loseit.fragments;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.robinhood.spark.SparkAdapter;
import com.robinhood.spark.SparkView;
import com.rocketstudio.dev.loseit.DatabaseClass;
import com.rocketstudio.dev.loseit.R;
import com.rocketstudio.dev.loseit.dialogs.SettingsDialog;
import com.rocketstudio.dev.loseit.helper.ChangeTheme;

import java.util.Random;

/**
 * Created by juliusherold on 14.03.17.
 */

public class DataFragment extends Fragment {

    private RandomizedAdapter adapter;

    String gewicht = "";
    String allGewicht ="";
    int allWerte=0;
    int werte = 0;
    int theme = 0;

    EditText kiloET,grammET;
    TextView kommaTV, scrubInfoTV, bmiTV, anzeigeTV;
    SparkView sparkView;
    ImageView saveIV,deleteIV;

    DatabaseClass databaseClass;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

         View rootView = inflater.inflate(R.layout.content_home, container, false);

        return rootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        findViews();

        Typeface face = Typeface.createFromAsset(getActivity().getAssets(),"fonts/robotolight.ttf");
        kiloET.setTypeface(face);
        grammET.setTypeface(face);
        kommaTV.setTypeface(face);

        databaseClass = new DatabaseClass(getActivity().getApplicationContext());

        try{
            Cursor cursor = databaseClass.getData(1);
            cursor.moveToFirst();

            gewicht = cursor.getString(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_WEIGHT));
            werte = cursor.getInt(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_ENTRIES));
            theme = cursor.getInt(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_THEME));

            int test = cursor.getInt(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_NOTIFICATION));

            changeTheme(theme, getView());

            adapter = new DataFragment.RandomizedAdapter(gewicht,werte);

            sparkView.setAdapter(adapter);

        }catch (Exception e){


            SettingsDialog settingsDialog = new SettingsDialog(getContext());
            settingsDialog.setCancelable(false);
            settingsDialog.show();
            settingsDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    Cursor cursor = databaseClass.getData(1);
                    cursor.moveToFirst();
                    int system = cursor.getInt(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_SYSTEM));
                    if (system == 1){
                        kiloET.setHint("kg");
                        grammET.setHint("gr");
                    }else {
                        kiloET.setHint("lb");
                        grammET.setHint("oz");
                    }

                    Cursor cursor1 = databaseClass.getData(1);
                    cursor1.moveToFirst();

                    gewicht = cursor1.getString(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_WEIGHT));
                    werte = cursor1.getInt(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_ENTRIES));

                    adapter = new DataFragment.RandomizedAdapter(gewicht,werte);

                    sparkView.setAdapter(adapter);
                }
            });



        }




        deleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                grammET.setText("");
                kiloET.setText("");
            }
        });


        saveIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (TextUtils.isEmpty(kiloET.getText()) || TextUtils.isEmpty(grammET.getText())){

                    if (TextUtils.isEmpty(kiloET.getText())){
                        YoYo.with(Techniques.Shake).duration(700).playOn(kiloET);
                    }if (TextUtils.isEmpty(grammET.getText())){
                        YoYo.with(Techniques.Shake).duration(700).playOn(grammET);
                    }


                }else {
                    try{

                        String weight = getWeight();
                        String date = getDate();


                        weight = weight + ";" + kiloET.getText().toString() + "." + grammET.getText().toString();
                        date = date + ";" + getTimeStamp();
                        int datenSaetze = getEntries();

                        kiloET.setText("");
                        grammET.setText("");


                        datenSaetze = datenSaetze +1;

                        databaseClass.updateData(1,weight,date,datenSaetze,theme);

                        allGewicht = getWeight();
                        allWerte = getEntries();

                        adapter = new RandomizedAdapter(allGewicht,allWerte);

                        sparkView.setAdapter(adapter);



                    }catch (Exception e){

                        String gewicht = kiloET.getText().toString() + "." + grammET.getText().toString();
                        String time = getTimeStamp();

                        kiloET.setText("");
                        grammET.setText("");


                        adapter = new RandomizedAdapter(allGewicht,allWerte);

                        sparkView.setAdapter(adapter);


                    }

                }

            }
        });




        adapter = new RandomizedAdapter(gewicht,werte);

        sparkView.setAdapter(adapter);

        sparkView.setScrubListener(new SparkView.OnScrubListener() {
            @Override
            public void onScrubbed(Object value) {
                if (value == null){
                    
                }else {

                    String kiloWert = value.toString().substring(0,value.toString().indexOf("."));
                    String grammWert = value.toString().substring(value.toString().indexOf(".")+1);
                    kiloET.setText(kiloWert);
                    grammET.setText(grammWert);

                    Cursor cursor = databaseClass.getData(1);
                    cursor.moveToFirst();

                    String test = cursor.getString(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_SIZE));

                    Double size = Double.valueOf(test);

                    Float gewicht = Float.valueOf(kiloWert + "." + grammWert);

                    Double ergebnis = gewicht/size/size;

                    ergebnis = Math.round(100.0 * ergebnis)/100.0;

                    if (ergebnis >=18.5 && ergebnis <= 25 ){
                        bmiTV.setTextColor(getResources().getColor(R.color.colorGreenLoseIt));
                        anzeigeTV.setTextColor(getResources().getColor(R.color.colorGreenLoseIt));
                    } else {
                        bmiTV.setTextColor(getResources().getColor(R.color.colorRed));
                        anzeigeTV.setTextColor(getResources().getColor(R.color.colorRed));
                    }

                    bmiTV.setText(ergebnis.toString());

                }
            }
        });

    }



    public String getTimeStamp(){
        Time time = new Time(Time.getCurrentTimezone());
        time.setToNow();

        String datum = time.monthDay + ":" + time.month + ":" + time.year;

        return datum;
    }


    private void findViews() {

        kiloET = (EditText) getActivity().findViewById(R.id.kiloET);
        grammET = (EditText) getActivity().findViewById(R.id.grammET);
        kommaTV = (TextView) getActivity().findViewById(R.id.kommaTV);
        sparkView = (SparkView) getActivity().findViewById(R.id.sparkview);
        saveIV = (ImageView) getActivity().findViewById(R.id.saveIV);
        deleteIV = (ImageView) getActivity().findViewById(R.id.deleteIV);
        bmiTV = (TextView) getActivity().findViewById(R.id.bmiTV);
        anzeigeTV = (TextView) getActivity().findViewById(R.id.anzeigeTV);


    }

    public String getWeight(){
        databaseClass = new DatabaseClass(getActivity().getApplicationContext());

        Cursor cursor = databaseClass.getData(1);
        cursor.moveToFirst();

        String weight = cursor.getString(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_WEIGHT));

        return weight;
    }

    private int getEntries(){
        databaseClass = new DatabaseClass(getActivity().getApplicationContext());

        Cursor cursor = databaseClass.getData(1);
        cursor.moveToFirst();

        int entries = cursor.getInt(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_ENTRIES));

        return entries;
    }


    private String getDate() {
        databaseClass = new DatabaseClass(getActivity().getApplicationContext());

        Cursor cursor = databaseClass.getData(1);
        cursor.moveToFirst();

        String weight = cursor.getString(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_TIMESTAMP));

        return weight;
    }

    public void setTheme (int themeNr){
        databaseClass.updateTheme(themeNr);
    }


    public void changeTheme(int theme, View view) {

        ChangeTheme changeTheme;
        changeTheme = new ChangeTheme(getActivity().getApplicationContext());

        switch (theme){
            case 1:
                changeTheme.setLoseIt(view);
                break;
            case 2:
                changeTheme.setBee(view);
                break;
            case 3:
                changeTheme.setBlack(view);
                break;

        }

    }



    public static class RandomizedAdapter extends SparkAdapter {
        private final float[] yData;
        private final Random random;
        String data="";
        int count=0;
        float weightDaten[];

        public RandomizedAdapter(String daten, int anzahl) {
            random = new Random();
            data = daten;
            count = anzahl;

            yData = getWeightArray();
            notifyDataSetChanged();
            //randomize();
        }

        public float[] getWeightArray(){

            String content = data;

            int gesamt = count;

            weightDaten = new float[gesamt];


            int position = 0;

            while (content.contains(".")){

                String wert = "";


                try{
                    wert = content.substring(0,content.indexOf(";"));
                }catch (Exception e){
                    wert = content;
                }


                float endgültig = Float.parseFloat(wert);

                weightDaten[position] = endgültig;


                position++;


                if (content.contains(";")){
                    content = content.substring(content.indexOf(";")+1);
                }else {
                    content = "";
                }


            }



            return weightDaten;
        }

        public void randomize() {
            for (int i = 0, count = yData.length; i < count; i++) {

                yData[i] = random.nextFloat();
            }
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return yData.length;
        }

        @Override
        public Object getItem(int index) {
            return yData[index];
        }

        @Override
        public float getY(int index) {
            return yData[index];
        }
    }

}
