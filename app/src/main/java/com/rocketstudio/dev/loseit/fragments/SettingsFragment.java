package com.rocketstudio.dev.loseit.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.rocketstudio.dev.loseit.R;
import com.rocketstudio.dev.loseit.controls.DataSettingContol;
import com.rocketstudio.dev.loseit.controls.ThemeSettingControl;

/**
 * Created by juliusherold on 14.03.17.
 */

public class SettingsFragment extends Fragment {

    DataSettingContol dataSC;
    ThemeSettingControl themeSC;
    AdView bannerAV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.settings_fragment_layout, container, false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findViews();

        AdRequest adRequest = new AdRequest.Builder().build();
        bannerAV.loadAd(adRequest);

    }



    private void findViews() {
        dataSC = (DataSettingContol) getActivity().findViewById(R.id.dataSC);
        themeSC = (ThemeSettingControl) getActivity().findViewById(R.id.themeSC);
        bannerAV = (AdView) getActivity().findViewById(R.id.bannerAV);
    }
}
