package com.rocketstudio.dev.loseit.controls

import android.content.Context
import android.database.Cursor
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.CompoundButton
import android.widget.LinearLayout
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast

import com.rocketstudio.dev.loseit.DatabaseClass
import com.rocketstudio.dev.loseit.R
import com.rocketstudio.dev.loseit.dialogs.SettingsDialog
import com.rocketstudio.dev.loseit.dialogs.SizeDialog

/**
 * Created by juliusherold on 17.03.17.
 */

class DataSettingContol : LinearLayout {

    private var headerLL: LinearLayout? = null
    private var dataLL: LinearLayout? = null
    private var heightTV: TextView? = null
    private val importTV: TextView? = null
    private var resetTV: TextView? = null
    internal var systemS: Switch
    internal var databaseClass: DatabaseClass


    constructor(context: Context) : super(context) {}

    constructor(context: Context, attributeSet: AttributeSet) : super(context) {
        initControl(context)
    }

    private fun initControl(context: Context) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        inflater.inflate(R.layout.data_control, this)

        findViews()

        databaseClass = DatabaseClass(getContext())





        headerLL!!.setOnClickListener {
            checkVisibility()
            setValue()
        }

        resetTV!!.setOnClickListener {
            val databaseClass: DatabaseClass

            databaseClass = DatabaseClass(context)

            databaseClass.deleteDatabase(context)

            val settingsDialog = SettingsDialog(getContext())
            settingsDialog.setCancelable(false)
            settingsDialog.show()
        }

        systemS.setOnCheckedChangeListener { compoundButton, b ->
            if (systemS.isChecked) {
                databaseClass.updateSystem(1, 1)
            } else {
                databaseClass.updateSystem(1, 0)
            }
        }

        heightTV!!.setOnClickListener {
            val sizeDialog = SizeDialog(getContext())
            sizeDialog.show()
        }


    }

    private fun setValue() {

        val cursor = databaseClass.getData(1)
        cursor.moveToFirst()

        val system = cursor.getInt(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_SYSTEM))

        if (system == 1) {
            systemS.isChecked = true
        } else {
            systemS.isChecked = false
        }

    }

    private fun findViews() {
        headerLL = findViewById(R.id.headerLL) as LinearLayout
        dataLL = findViewById(R.id.dataLL) as LinearLayout
        heightTV = findViewById(R.id.heightTV) as TextView
        resetTV = findViewById(R.id.resetTV) as TextView
        systemS = findViewById(R.id.einheitS) as Switch
    }


    fun checkVisibility() {
        if (dataLL!!.visibility == View.VISIBLE) {
            dataLL!!.visibility = View.GONE
        } else {
            dataLL!!.visibility = View.VISIBLE
        }
    }

}
