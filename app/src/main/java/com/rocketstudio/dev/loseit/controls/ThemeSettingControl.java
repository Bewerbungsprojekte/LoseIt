package com.rocketstudio.dev.loseit.controls;

import android.content.Context;
import android.database.Cursor;
import android.media.session.PlaybackState;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.rocketstudio.dev.loseit.DatabaseClass;
import com.rocketstudio.dev.loseit.MainActivity;
import com.rocketstudio.dev.loseit.R;
import com.rocketstudio.dev.loseit.fragments.DataFragment;

/**
 * Created by juliusherold on 14.03.17.
 */

public class ThemeSettingControl extends LinearLayout {


    private LinearLayout headerLL,themesLL;
    private HorizontalScrollView themesHSV;
    DatabaseClass databaseClass;


    public ThemeSettingControl(Context context, AttributeSet st) {
        super(context,st);
        initControl(context);
    }

    public ThemeSettingControl(Context context) {
        super(context);
        initControl(context);
    }

    private void initControl(final Context context){

        databaseClass = new DatabaseClass(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        inflater.inflate(R.layout.theme_control, this);

        findViews();

        headerLL.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                checkVisibility();
            }
        });

       LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(HORIZONTAL);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        for (int i = 0; i<3;i++){
            final ThemeDisplayControl tdc = new ThemeDisplayControl(context);
            tdc.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
            tdc.setName(i);
            tdc.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                    String test = tdc.getName();
                    int theme = 0;


                    if (test.equals("Lose It")){
                        theme = 1;
                    }else if (test.equals("The Bee")){
                        theme = 2;
                    }else if (test.equals("Red")){
                        theme = 3;
                    }


                    Cursor cursor = databaseClass.getData(1);
                    cursor.moveToFirst();

                    String gewicht = cursor.getString(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_WEIGHT));
                    String datum = cursor.getString(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_TIMESTAMP));
                    int datensätze = cursor.getInt(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_ENTRIES));

                    databaseClass.updateData(1,gewicht,datum,datensätze,theme);


                    Toast.makeText(context,"Restart LoseIt", Toast.LENGTH_SHORT).show();

                }
            });
            tdc.setPreview(i);
            linearLayout.addView(tdc);
        }


        themesHSV.addView(linearLayout);


    }

    private void checkVisibility(){



        if (themesLL.getVisibility() == VISIBLE){
            themesLL.setVisibility(GONE);
        }else {

            themesLL.setVisibility(VISIBLE);
        }
    }

    private void findViews() {
        headerLL = (LinearLayout) findViewById(R.id.headerLL);
        themesLL = (LinearLayout) findViewById(R.id.themesLL);
        themesHSV = (HorizontalScrollView) findViewById(R.id.themesHSV);
    }
}
