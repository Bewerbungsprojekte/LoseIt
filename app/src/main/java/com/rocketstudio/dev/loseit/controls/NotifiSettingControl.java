package com.rocketstudio.dev.loseit.controls;

import android.content.Context;
import android.database.Cursor;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.rocketstudio.dev.loseit.DatabaseClass;
import com.rocketstudio.dev.loseit.R;
import com.rocketstudio.dev.loseit.helper.AlarmHelper;

/**
 * Created by herold on 30.03.2017.
 */

public class NotifiSettingControl extends LinearLayout {
    
    LinearLayout headerLL, notificationLL;
    Switch alarmS;
    Context context;

    DatabaseClass databaseClass;

    public NotifiSettingControl(Context context, AttributeSet st) {
        super(context,st);
        initControl(context);
    }

    public NotifiSettingControl(Context context) {
        super(context);
        initControl(context);
    }

    private void initControl(Context context) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try {
            inflater.inflate(R.layout.notification_control, this);
        }catch (Exception e){
            e.printStackTrace();
        }


        this.context = context;
        try {
            findViews();
        }catch (Exception e){
            e.printStackTrace();
        }


        onClicks();

    }

    private void setValue() {
        databaseClass = new DatabaseClass(context);

        Cursor cursor = databaseClass.getData(1);
        cursor.moveToFirst();

        int state = cursor.getInt(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_NOTIFICATION));

        if (state == 0){
            alarmS.setChecked(false);
        }else {
            alarmS.setChecked(true);
        }
    }

    private void onClicks() {
        headerLL.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                checkVisibility();
                setValue();
            }
        });

        alarmS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (alarmS.isChecked()){
                    databaseClass.updateNotification(1,1);

                    Cursor cursor = databaseClass.getData(1);
                    cursor.moveToFirst();

                    int time = cursor.getInt(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_TIME));

                    AlarmHelper.getInstance(context).setNotification(time);
                }else {
                    databaseClass.updateNotification(1,0);
                    AlarmHelper.getInstance(context).deleteNotification();
                }
            }
        });
    }

    private void checkVisibility() {
        if (notificationLL.getVisibility() == GONE){
            notificationLL.setVisibility(VISIBLE);
        }else {
            notificationLL.setVisibility(GONE);
        }
    }

    private void findViews() {
        headerLL = (LinearLayout) findViewById(R.id.headerLL);
        notificationLL = (LinearLayout) findViewById(R.id.notificationLL);
        alarmS = (Switch) findViewById(R.id.alarmS);
    }

}
