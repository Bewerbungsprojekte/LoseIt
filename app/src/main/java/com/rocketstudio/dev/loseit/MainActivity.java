package com.rocketstudio.dev.loseit;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.widget.Toast;

import com.rocketstudio.dev.loseit.fragments.DataFragment;

/**
 * Created by juliusherold on 14.03.17.
 */

public class MainActivity extends FragmentActivity {

    private ViewPager viewPager;
    private FragmentPageAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAdapter = new FragmentPageAdapter(getSupportFragmentManager());

        viewPager = (ViewPager) findViewById(R.id.inhaltVP);
        viewPager.setAdapter(mAdapter);

    }




}
