package com.rocketstudio.dev.loseit;

import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.format.Time;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.robinhood.spark.SparkAdapter;
import com.robinhood.spark.SparkView;
import com.rocketstudio.dev.loseit.fragments.DataFragment;
import com.rocketstudio.dev.loseit.fragments.SettingsFragment;

import java.lang.reflect.Array;
import java.util.Random;

public class FragmentPageAdapter extends FragmentPagerAdapter {


    public FragmentPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                return new DataFragment();
            case 1:
                return new SettingsFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
