package com.rocketstudio.dev.loseit.controls;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.rocketstudio.dev.loseit.DatabaseClass;
import com.rocketstudio.dev.loseit.MainActivity;
import com.rocketstudio.dev.loseit.R;
import com.rocketstudio.dev.loseit.helper.AlarmHelper;
import com.rocketstudio.dev.loseit.helper.AlarmReciever;

import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by juliusherold on 23.03.17.
 */

public class CounterNotificationControl extends RelativeLayout {

    TextView plusTV, minusTV, clockTV;
    DatabaseClass databaseClass;

    Switch alarmS;
    int time,notification;
    Context context;

    public CounterNotificationControl(Context context, AttributeSet attributeSet){
        super(context,attributeSet);
        this.context = context;
        initControl(context);
    }

    public CounterNotificationControl(Context context){
        super(context);
    }


    private void initControl(Context context) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try {
            inflater.inflate(R.layout.counter_control, this);
        }catch (Exception e){
            e.printStackTrace();
        }


        findViews();

        try {
            setValues();

            onClicks();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void setValues() {
        databaseClass = new DatabaseClass(context);

        Cursor cursor = databaseClass.getData(1);
        cursor.moveToFirst();

        int bisher = cursor.getInt(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_TIME));
        clockTV.setText(bisher+"");

    }


    private void onClicks() {

        databaseClass = new DatabaseClass(context);

        final Cursor cursor = databaseClass.getData(1);
        cursor.moveToFirst();

        plusTV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                int aktuell = Integer.parseInt(clockTV.getText().toString());
                if (aktuell == 24){
                    aktuell = 0;
                }else {
                    aktuell++;
                }

                clockTV.setText(aktuell+"");
                databaseClass.updateTime(1,aktuell);

                int state = cursor.getInt(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_NOTIFICATION));

                if (state == 1){
                    AlarmHelper.getInstance(context).setNotification(aktuell);
                }






            }
        });

        minusTV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                int aktuell = Integer.parseInt(clockTV.getText().toString());
                if (aktuell == 0){
                    aktuell = 24;
                }else {
                    aktuell--;
                }
                clockTV.setText(aktuell+"");
                databaseClass.updateTime(1,aktuell);

                int state = cursor.getInt(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_NOTIFICATION));

                if (state == 1){
                    AlarmHelper.getInstance(context).setNotification(aktuell);
                }
            }
        });
    }


    private void findViews() {
        plusTV = (TextView) findViewById(R.id.plusTV);
        minusTV = (TextView) findViewById(R.id.minusTV);
        clockTV = (TextView) findViewById(R.id.clockTV);
        alarmS = (Switch) findViewById(R.id.alarmS);
    }
}
