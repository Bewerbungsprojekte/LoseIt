package com.rocketstudio.dev.loseit;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * Created by herold on 02.03.2017.
 */

public class DatabaseClass extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyData.db";
    public static final String DATA_TABLE_NAME = "data";
    public static final String DATA_COLUMN_ID = "id";
    public static final String DATA_COLUMN_WEIGHT = "weight";
    public static final String DATA_COLUMN_TIMESTAMP = "timestamp";
    public static final String DATA_COLUMN_ENTRIES = "entries";
    public static final String DATA_COLUMN_THEME = "theme";
    public static final String DATA_COLUMN_NOTIFICATION = "notification";
    public static final String DATA_COLUMN_TIME = "time";
    public static final String DATA_COLUMN_SIZE = "size";
    public static final String DATA_COLUMN_SYSTEM = "system";



    public DatabaseClass(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + DATA_TABLE_NAME + "(" +
                DATA_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                DATA_COLUMN_WEIGHT + " TEXT, " +
                DATA_COLUMN_TIMESTAMP + " TEXT, " +
                DATA_COLUMN_ENTRIES + " INTEGER, " +
                DATA_COLUMN_THEME + " INTEGER, " +
                DATA_COLUMN_NOTIFICATION + " INTEGER, " +
                DATA_COLUMN_TIME + " INTEGER, " +
                DATA_COLUMN_SIZE + " TEXT, " +
                DATA_COLUMN_SYSTEM + " INTEGER)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS data");
        onCreate(sqLiteDatabase);
    }

    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + DATA_TABLE_NAME + " WHERE " +
                DATA_COLUMN_ID + "=?", new String[]{Integer.toString(id)});
        return res;
    }

    public boolean updateData(Integer id, String weight, String timestamp, int entries, int theme) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DATA_COLUMN_WEIGHT, weight);
        contentValues.put(DATA_COLUMN_TIMESTAMP, timestamp);
        contentValues.put(DATA_COLUMN_ENTRIES, entries);
        contentValues.put(DATA_COLUMN_THEME, theme);
        db.update(DATA_TABLE_NAME, contentValues, DATA_COLUMN_ID + " = ? ", new String[]{Integer.toString(id)});
        return true;
    }


    public boolean insertData(String weight,String date, int entries, int themes, int notification, int time, String size, int system) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DATA_COLUMN_WEIGHT, weight);
        contentValues.put(DATA_COLUMN_TIMESTAMP, date);
        contentValues.put(DATA_COLUMN_ENTRIES, entries);
        contentValues.put(DATA_COLUMN_THEME, themes);
        contentValues.put(DATA_COLUMN_NOTIFICATION, notification);
        contentValues.put(DATA_COLUMN_TIME, time);
        contentValues.put(DATA_COLUMN_SIZE, size);
        contentValues.put(DATA_COLUMN_SYSTEM, system);
        try{
            db.insert(DATA_TABLE_NAME, null, contentValues);
        }catch (Exception e){
            e.printStackTrace();
        }

        return true;
    }

    public boolean updateTheme(int theme){
        SQLiteDatabase db =getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DATA_COLUMN_THEME,theme);
        try{
            db.insert(DATA_TABLE_NAME, null, contentValues);
        }catch (Exception e){
            e.printStackTrace();
        }

        return true;
    }

    public boolean updateNotification(int id ,int notification){
        SQLiteDatabase db =getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DATA_COLUMN_NOTIFICATION,notification);
        db.update(DATA_TABLE_NAME, contentValues, DATA_COLUMN_ID + " = ? ", new String[]{Integer.toString(id)});


        return true;
    }

    public boolean updateTime(int id, int time){
        SQLiteDatabase db =getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DATA_COLUMN_TIME,time);
        db.update(DATA_TABLE_NAME, contentValues, DATA_COLUMN_ID + " = ? ", new String[]{Integer.toString(id)});

        return true;
    }

    public boolean updateSize(int id, String size){
        SQLiteDatabase db =getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DATA_COLUMN_SIZE,size);
        db.update(DATA_TABLE_NAME, contentValues, DATA_COLUMN_ID + " = ? ", new String[]{Integer.toString(id)});

        return true;
    }

    public boolean updateSystem(int id, int system){
        SQLiteDatabase db =getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DATA_COLUMN_SYSTEM,system);
        db.update(DATA_TABLE_NAME, contentValues, DATA_COLUMN_ID + " = ? ", new String[]{Integer.toString(id)});

        return true;
    }

    public String deleteDatabase(Context context){
        context.deleteDatabase(DATABASE_NAME);
        return "Daten wurden gelöscht";
    }


}
