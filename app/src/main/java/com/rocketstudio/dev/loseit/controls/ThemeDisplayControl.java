package com.rocketstudio.dev.loseit.controls;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rocketstudio.dev.loseit.R;

import java.lang.reflect.Field;

/**
 * Created by herold on 15.03.2017.
 */

public class ThemeDisplayControl extends LinearLayout {

    ImageView previewIV;
    TextView nameTV;

    String[] namen ={"Lose It","The Bee","Red"};
    int[] previews = {R.drawable.loseittheme,R.drawable.thebeetheme,R.drawable.redtheme};

    public ThemeDisplayControl(Context context, AttributeSet attrs) {
        super(context, attrs);
        initControl(context);
    }

    public ThemeDisplayControl(Context context) {
        super(context);
        initControl(context);
    }

    private void initControl(Context context) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        inflater.inflate(R.layout.theme_display_control, this);

        findViews();

    }

    private void findViews() {
        previewIV = (ImageView) findViewById(R.id.previewIV);
        nameTV = (TextView) findViewById(R.id.nameTV);
    }

    public String getName(){
       String name = nameTV.getText().toString();
        return name;
    }

    public void setName(int position){
        nameTV.setText(namen[position]);
    }

    public void setPreview(int position){
        int res = previews[position];
        previewIV.setBackgroundResource(res);
    }


}
