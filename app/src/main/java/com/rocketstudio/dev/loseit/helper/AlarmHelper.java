package com.rocketstudio.dev.loseit.helper;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

/**
 * Created by juliusherold on 28.03.17.
 */

public class AlarmHelper {

    AlarmManager am;
    PendingIntent pendingIntent;
    Context context;

    private static AlarmHelper _instance;

    public static AlarmHelper getInstance(Context context){
        if (_instance == null){
            _instance = new AlarmHelper(context);
        }
        return _instance;
    }

    public AlarmHelper(Context context){

        this.context = context;

    }

    public void setNotification(int value) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, value);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Intent intent1 = new Intent(context, AlarmReciever.class);

        pendingIntent = PendingIntent.getBroadcast(context, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        am  = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);

    }

    public void deleteNotification(){
        Intent intent = new Intent(context, AlarmReciever.class);
        pendingIntent = PendingIntent.getBroadcast(context,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        am = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
        am.cancel(pendingIntent);
    }

}
