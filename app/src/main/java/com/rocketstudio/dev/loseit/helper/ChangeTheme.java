package com.rocketstudio.dev.loseit.helper;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.robinhood.spark.SparkView;
import com.rocketstudio.dev.loseit.R;

/**
 * Created by juliusherold on 21.03.17.
 */

public class ChangeTheme {

    LinearLayout baseLL;
    SparkView chartSV;
    EditText kiloET,grammET;
    ImageView saveIV;
    TextView kommaTV, anzeigeTV;
    Context context;

    public ChangeTheme(Context context){
        this.context = context;
    }


    public void setBee(View view){

        baseLL = (LinearLayout) view.findViewById(R.id.content_home);
        chartSV = (SparkView) view.findViewById(R.id.sparkview);
        kiloET = (EditText) view.findViewById(R.id.kiloET);
        grammET = (EditText) view.findViewById(R.id.grammET);
        kommaTV = (TextView) view.findViewById(R.id.kommaTV);
        saveIV = (ImageView) view.findViewById(R.id.saveIV);
        anzeigeTV = (TextView) view.findViewById(R.id.anzeigeTV);

        baseLL.setBackgroundColor(context.getResources().getColor(R.color.colorBlack));
        chartSV.setLineColor(context.getResources().getColor(R.color.colorYellow));
        kiloET.setTextColor(context.getResources().getColor(R.color.colorYellow));
        grammET.setTextColor(context.getResources().getColor(R.color.colorYellow));
        kommaTV.setTextColor(context.getResources().getColor(R.color.colorYellow));
        anzeigeTV.setTextColor(context.getResources().getColor(R.color.colorYellow));
        saveIV.setImageResource(R.drawable.saveyellow);

    }

    public void setLoseIt(View view) {

        baseLL = (LinearLayout) view.findViewById(R.id.content_home);
        chartSV = (SparkView) view.findViewById(R.id.sparkview);
        kiloET = (EditText) view.findViewById(R.id.kiloET);
        grammET = (EditText) view.findViewById(R.id.grammET);
        kommaTV = (TextView) view.findViewById(R.id.kommaTV);
        saveIV = (ImageView) view.findViewById(R.id.saveIV);
        anzeigeTV = (TextView) view.findViewById(R.id.anzeigeTV);

        baseLL.setBackgroundColor(context.getResources().getColor(R.color.colorDefault));
        chartSV.setLineColor(context.getResources().getColor(R.color.colorGreenLoseIt));
        kiloET.setTextColor(context.getResources().getColor(R.color.colorGreenLoseIt));
        grammET.setTextColor(context.getResources().getColor(R.color.colorGreenLoseIt));
        kommaTV.setTextColor(context.getResources().getColor(R.color.colorGreenLoseIt));
        anzeigeTV.setTextColor(context.getResources().getColor(R.color.colorGreenLoseIt));
        saveIV.setImageResource(R.drawable.save);

    }

    public void setBlack(View view){
        baseLL = (LinearLayout) view.findViewById(R.id.content_home);
        chartSV = (SparkView) view.findViewById(R.id.sparkview);
        kiloET = (EditText) view.findViewById(R.id.kiloET);
        grammET = (EditText) view.findViewById(R.id.grammET);
        kommaTV = (TextView) view.findViewById(R.id.kommaTV);
        saveIV = (ImageView) view.findViewById(R.id.saveIV);
        anzeigeTV = (TextView) view.findViewById(R.id.anzeigeTV);

        baseLL.setBackgroundColor(context.getResources().getColor(R.color.colorBlack));
        chartSV.setLineColor(context.getResources().getColor(R.color.colorRed));
        kiloET.setTextColor(context.getResources().getColor(R.color.colorRed));
        grammET.setTextColor(context.getResources().getColor(R.color.colorRed));
        kommaTV.setTextColor(context.getResources().getColor(R.color.colorRed));
        anzeigeTV.setTextColor(context.getResources().getColor(R.color.colorRed));
        saveIV.setImageResource(R.drawable.savered);
    }
}
