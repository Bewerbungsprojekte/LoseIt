package com.rocketstudio.dev.loseit.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.rocketstudio.dev.loseit.DatabaseClass;
import com.rocketstudio.dev.loseit.R;

/**
 * Created by herold on 03.04.2017.
 */

public class SizeDialog extends Dialog {

    EditText size1ET, size2ET;
    Button saveB;
    DatabaseClass databaseClass;

    public SizeDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.size_dialog);

        databaseClass = new DatabaseClass(getContext());
        findViews();
        setContent();
        onClicks();
    }

    private void setContent() {
        Cursor cursor = databaseClass.getData(1);
        cursor.moveToFirst();

        int system = cursor.getInt(cursor.getColumnIndex(DatabaseClass.DATA_COLUMN_SYSTEM));


        if (system == 1){
            size1ET.setHint("m");
            size2ET.setHint("cm");
        }else {
            size1ET.setHint("in");
            size2ET.setHint("ft");
        }

    }

    private void findViews() {
        size1ET = (EditText) findViewById(R.id.size1ET);
        size2ET = (EditText) findViewById(R.id.size2ET);
        saveB = (Button) findViewById(R.id.saveB);

    }

    private void onClicks() {
        saveB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(size1ET.getText()) || TextUtils.isEmpty(size2ET.getText())){
                    if (TextUtils.isEmpty(size1ET.getText())){
                        YoYo.with(Techniques.Shake).duration(700).playOn(size1ET);
                    }if (TextUtils.isEmpty(size2ET.getText())){
                        YoYo.with(Techniques.Shake).duration(700).playOn(size2ET);
                    }
                }else {


                    String groese = size1ET.getText() + "." + size2ET.getText();

                    databaseClass.updateSize(1,groese);
                    dismiss();
                }
            }
        });
    }
}
